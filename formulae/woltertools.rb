require 'formula'

class Woltertools < Formula
  
  url 'https://bitbucket.org/bww/woltertools.git', :using => :git
  version '1'
  
  def install
    system "xcodebuild", "-project", "WolterTools.xcodeproj", "-target", "WolterTools", "-configuration", "Release", "clean", "install", "SYMROOT=build", "DSTROOT=build", "INSTALL_PATH=/bin"
    bin.install "build/bin/plist"
    bin.install "build/bin/walk"
  end
  
end