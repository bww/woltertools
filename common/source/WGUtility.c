// 
// Copyright 2013 Brian William Wolter, All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// 

#import "WGUtility.h"

/**
 * Escape a string
 * 
 * @param dest destination string buffer
 * @param src source string
 * @param len destination buffer length
 * @return actual destination string length
 */
int WGEscapeString(char *dest, const char *src, size_t len) {
  
  size_t vcount = 4;
  WGRange valid[] = {
    WGRangeMake(43, (59 - 43)),
    WGRangeMake(65, (90 - 65)),
    WGRangeMake(95, 1),
    WGRangeMake(97, (122 - 97))
  };
  
  char *dp = dest;
  const char *sp = src;
  const char echar = '\\';
  int esc = 0;
  
  int i = 0;
  while(i < (len - 1)){
    char v;
    
    if((v = *sp++) == 0)
      break;
    
    int isvalid = 0;
    register int j;
    for(j = 0; j < vcount; j++){
      if(WGRangeContainsValue(valid[j], v)){
        isvalid = 1;
        break;
      }
    }
    
    if(!isvalid){
      if((esc % 2) == 0){ *dp++ = echar; i++; }
      esc = 0;
    }else if(v == echar){
      esc++;
    }else{
      esc = 0;
    }
    
    if(i++ < (len - 1)) *dp++ = v;
  }
  
  *dp = 0;
  return i;
}

