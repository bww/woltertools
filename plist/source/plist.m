// 
// Copyright 2013 Brian William Wolter, All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// 

#import <Foundation/Foundation.h>

#import <unistd.h>
#import <sys/param.h>
#import <getopt.h>

enum {
  kWGOptionNone           = 0,
  kWGOptionQuoteOutput    = 1 << 0,
  kWGOptionEscapeOutput   = 1 << 1,
  kWGOptionWrite          = 1 << 2,
  kWGOptionDelete         = 1 << 3
};

typedef enum WGKeyType_ {
  kWGKeyTypeUnknown   = -1,
  kWGKeyTypeString    =  0,
  kWGKeyTypeNumber,
  kWGKeyTypeBoolean
} WGKeyType;

const char *kWGKeyTypeNames[] = {
  "string",
  "number",
  "boolean",
  NULL
};

int WGPropertyListRead(const char *command, int argc, char *argv[], int options, char *delimiter);
int WGPropertyListWrite(const char *command, int argc, char *argv[], int options);
int WGPropertyListDelete(const char *command, int argc, char *argv[], int options);
void WGPropertyListUsage(const char *command, FILE *stream);

int main(int argc, char *argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	char *command = argv[0];
  int options = kWGOptionNone;
  int flag = 0;
  char *delimiter = "\n";
	
  static struct option longopts[] = {
    { "read",         no_argument,        NULL,         'r' },  // read values to the plist (this is the default)
    { "write",        no_argument,        NULL,         'w' },  // write a value to the plist instead of reading
    { "delete",       no_argument,        NULL,         'D' },  // delete a value from the plist instead of reading
    { "quote",        no_argument,        NULL,         'Q' },  // quote output strings
    { "escape",       no_argument,        NULL,         'e' },  // escape output strings
    { "delimiter",    required_argument,  NULL,         'd' },  // element delimiter
    { NULL,           0,                  NULL,          0  }
  };
  
  while((flag = getopt_long(argc, (char **)argv, "rwDQed:", longopts, NULL)) != -1){
    switch(flag){
      
      case 'r':
        options = options & ~(kWGOptionWrite | kWGOptionDelete);
        break;
        
      case 'w':
        options |= kWGOptionWrite;
        break;
        
      case 'D':
        options |= kWGOptionDelete;
        break;
        
      case 'Q':
        options |= kWGOptionQuoteOutput;
        break;
        
      case 'e':
        options |= kWGOptionEscapeOutput;
        break;
        
      case 'd':
        delimiter = optarg;
        break;
        
      default:
        exit(0);
        
    }
  }
  
  argv += optind;
  argc -= optind;
  
  if((options & kWGOptionWrite) == kWGOptionWrite){
    WGPropertyListWrite(command, argc, argv, options);
  }else if((options & kWGOptionDelete) == kWGOptionDelete){
    WGPropertyListDelete(command, argc, argv, options);
  }else{
    WGPropertyListRead(command, argc, argv, options, delimiter);
  }
  
  [pool release];
  return 0;
}

int WGPropertyListRead(const char *command, int argc, char *argv[], int options, char *delimiter) {
  NSDictionary *dict = nil;
  
	if(argc < 2){
	  WGPropertyListUsage(command, stderr);
	  goto error;
	}
	
	NSString *docpath = [NSString stringWithUTF8String:argv[0]];
	
	if((dict = [[NSDictionary alloc] initWithContentsOfFile:docpath]) == nil){
    fprintf(stderr, "Unable to read property list: %s\n", argv[0]);
    goto error;
	}
  
	register int i;
	for(i = 1; i < argc; i++){
	  
    id value;
    if((value = [dict valueForKeyPath:[NSString stringWithUTF8String:argv[i]]]) != nil){
      if((options & kWGOptionQuoteOutput) == kWGOptionQuoteOutput) fputs("\"", stdout);
      if((options & kWGOptionEscapeOutput) == kWGOptionEscapeOutput){
        CFStringRef string = (CFStringRef)[value description];
        CFMutableStringRef buffer = CFStringCreateMutable(NULL, 0);
        CFCharacterSetRef escape = CFCharacterSetCreateWithCharactersInString(NULL, CFSTR("\" "));
        UniChar prefix = '\\';
        int esc = 0;
        
        register int j;
        for(j = 0; j < CFStringGetLength(string); j++){
          UniChar v = CFStringGetCharacterAtIndex(string, j);
          
          if(CFCharacterSetIsCharacterMember(escape, v)){
            if((esc % 2) == 0) CFStringAppendCharacters(buffer, &prefix, 1);
            esc = 0;
          }else if(v == prefix){
            esc++;
          }else{
            esc = 0;
          }
          
          CFStringAppendCharacters(buffer, &v, 1);
        }
        
        fputs([(NSString *)buffer UTF8String], stdout);
        
        if(buffer) CFRelease(buffer);
        if(escape) CFRelease(escape);
      }else{
        fputs([[value description] UTF8String], stdout);
      }
      if((options & kWGOptionQuoteOutput) == kWGOptionQuoteOutput) fputs("\"", stdout);
    }
    
    if(i + 1 < argc){
      fputs(delimiter, stdout);
    }
    
  }
	
  fputs("\n", stdout);
  
error:
	[dict release];
  
  return 0;
}

int WGPropertyListWrite(const char *command, int argc, char *argv[], int options) {
	NSMutableDictionary *dict = nil;
  
	if(argc < 4 || ((argc - 1) % 3) != 0){
	  WGPropertyListUsage(command, stderr);
	  goto error;
	}
	
	NSString *docpath = [NSString stringWithUTF8String:argv[0]];
	
	NSDictionary *temporary;
	if((temporary = [[[NSDictionary alloc] initWithContentsOfFile:docpath] autorelease]) == nil){
    dict = [[NSMutableDictionary alloc] init];
	}else{
	  dict = [temporary mutableCopy];
	}
  
	register int i;
	for(i = 1; i < argc; i++){
    
    NSString *path = [NSString stringWithUTF8String:argv[i++]];
    WGKeyType type = kWGKeyTypeUnknown;
    id value = nil;
    
    if(i > (argc - 2)){
      fprintf(stderr, "Keys, types and values are not balanced\n");
      goto error;
    }
    
    const char *typename;
    for(int j = 0; (typename = kWGKeyTypeNames[j]) != NULL; j++){
      if(strcasecmp(kWGKeyTypeNames[j], argv[i]) == 0){
        type = j;
        break;
      }
    }
    
    if(type == kWGKeyTypeUnknown){
      fprintf(stderr, "Unknown type: %s (interpreting as string)\n", argv[i]);
      type = kWGKeyTypeString;
    }
    
    switch(type){
      case kWGKeyTypeNumber:
        value = [NSNumber numberWithDouble:[[NSString stringWithUTF8String:argv[++i]] doubleValue]];
        break;
      case kWGKeyTypeBoolean:
        value = [NSNumber numberWithBool:[[NSString stringWithUTF8String:argv[++i]] boolValue]];
        break;
      case kWGKeyTypeString:
      default:
        value = [NSString stringWithUTF8String:argv[++i]];
        break;
    }
    
    [dict setValue:value forKeyPath:path];
    
  }
	
  [dict writeToFile:docpath atomically:TRUE];
  
error:
	[dict release];
  
  return 0;
}

int WGPropertyListDelete(const char *command, int argc, char *argv[], int options) {
	NSMutableDictionary *dict = nil;
  
	if(argc < 2){
	  WGPropertyListUsage(command, stderr);
	  goto error;
	}
	
	NSString *docpath = [NSString stringWithUTF8String:argv[0]];
	
	NSDictionary *temporary;
	if((temporary = [[[NSDictionary alloc] initWithContentsOfFile:docpath] autorelease]) == nil){
    dict = [[NSMutableDictionary alloc] init];
	}else{
	  dict = [temporary mutableCopy];
	}
  
	register int i;
	for(i = 1; i < argc; i++){
	  NSString *path = [NSString stringWithUTF8String:argv[i]];
	  NSArray *parts = [path componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
	  
	  id deref = dict;
	  for(int i = 0; i < [parts count] - 1; i++){
	    deref = [deref valueForKey:[parts objectAtIndex:i]];
	  }
	  
    [dict removeObjectForKey:[parts objectAtIndex:[parts count] - 1]];
  }
	
  [dict writeToFile:docpath atomically:TRUE];
  
error:
	[dict release];
  
  return 0;
}

/**
 * Display usage information
 */
void WGPropertyListUsage(const char *command, FILE *stream) {
  fprintf(stream, "Usage: %s [-d <delimiter>] [-Qe] [--read] <file> <key path> ...\n", command);
  fprintf(stream, "       %s [-d <delimiter>] [-Qe] --write <file> <key path> <key type> <key value> ...\n", command);
  fprintf(stream, "       %s [-d <delimiter>] [-Qe] --delete <file> <key path> ...\n", command);
}



