// 
// Copyright 2013 Brian William Wolter, All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// 

#include <CoreFoundation/CoreFoundation.h>

#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>

#include "WGUtility.h"

#define kPathSeparator            '/'
#define kPathSeparatorString      "/"
#define kDefaultResultSeparator   "\n"
#define kEveryType                0xffffffff

typedef enum {
  kOptionVerbose        = 1 << 0,
  kOptionEscapeStrings  = 1 << 1,
  kOptionQuoteStrings   = 1 << 2,
  kOptionPrefixSize     = 1 << 3,
  kOptionIgnoreHidden   = 1 << 4,
  kOptionRequiresStat   = kOptionPrefixSize
} OptionFlags;

typedef struct WGWalkOptions_ {
  
  const char  *command;
  const char  *separator;
  int         flags;
  int         includeTypes;
  int         maxDepth;
  
} WGWalkOptions;

WGWalkOptions * WGWalkLoadOptions(WGWalkOptions *options, int argc, const char *argv[]);
void WGWalkDisplayUsage(FILE *stream);
void WGWalkPath(WGWalkOptions *options, const char *path, int depth, FILE *stream);
int WGEscapeString(char *dest, const char *src, size_t len);

/**
 * Main
 */
int main(int argc, const char *argv[]){
  
  WGWalkOptions options;
  WGWalkLoadOptions(&options, argc, argv);
  
  if(optind >= argc){
    WGWalkPath(&options, ".", 0, stdout);
  }else{
    while(optind < argc)
      WGWalkPath(&options, argv[optind++], 0, stdout);
  }
  
  return 0;
}

/**
 * Load options
 * 
 * @param options on output, options
 * @param argc argument count
 * @param argv arguments
 * @return input options pointer
 */
WGWalkOptions * WGWalkLoadOptions(WGWalkOptions *options, int argc, const char *argv[]) {
  int flag;
  
  bzero(options, sizeof(WGWalkOptions));
  
  options->command = argv[0];
  options->separator = kDefaultResultSeparator;
  options->includeTypes = kEveryType;
  options->maxDepth = -1;
  
  static struct option longopts[] = {
    { "separator",    required_argument,  NULL,         'S' },
    { "dir",          no_argument,        NULL,         'd' },
    { "no-dir",       no_argument,        NULL,         'D' },
    { "no-hidden",    no_argument,        NULL,         'H' },
    { "max-depth",    required_argument,  NULL,         'n' },
    { "escape",       no_argument,        NULL,         'x' },
    { "quote",        no_argument,        NULL,         'q' },
    { "prefix-size",  no_argument,        NULL,         'k' },
    { "help",         no_argument,        NULL,         'h' },
    { NULL,           0,                  NULL,          0  }
  };
  
  while((flag = getopt_long(argc, (char **)argv, "kdDHxqn:S:h", longopts, NULL)) != -1){
    switch(flag){
      
      case 'd':
        options->includeTypes = DT_DIR;
        break;
        
      case 'D':
        options->includeTypes = options->includeTypes & (~DT_DIR);
        break;
        
      case 'H':
        options->flags |= kOptionIgnoreHidden;
        break;
        
      case 'x':
        options->flags |= kOptionEscapeStrings;
        break;
        
      case 'q':
        options->flags |= kOptionQuoteStrings;
        break;
        
      case 'k':
        options->flags |= kOptionPrefixSize;
        break;
        
      case 'n':
        options->maxDepth = atoi(optarg);
        break;
        
      case 'S':
        options->separator = optarg;
        break;
        
      case 'h':
      default:
        WGWalkDisplayUsage(stdout);
        exit(0);
        
    }
  }
  
  return options;
}

/**
 * Display useage information
 * 
 * @param stream file stream to print to
 */
void WGWalkDisplayUsage(FILE *stream) {
  
  fputs("Walk - Recursively traverse and display files\n", stream);
  fputs("Copyright 2008, 2009 Wolter Group New York, Inc., All rights reserved.\n", stream);
  fputs(
    "walk [options] <path> [<path> ...]\n"
    "\n"
    "Options:\n"
    "   -d --dir                  List only directories\n"
    "   -D --no-dir               Don't list directories\n"
    "   -H --no-hidden            Don't list hidden (dot-prefixed) files or directories\n"
    "   -S --separator <string>   Specify a string to separate results (default: \\n)\n"
    "   -n --max-depth <number>   Specify the maximum depth to descend to (default: unlimited)\n"
    "   -x --escape               Escape special characters in output strings\n"
    "   -q --quote                Quote each output line\n"
    "   -k --prefix-size          Prefix output lines with the file's size, in bytes\n"
    "   -h --help                 Display this help information\n"
    "\n",
    stream
    );
  
}

/**
 * Walk a path
 * 
 * @param options options
 * @param path path to traverse
 * @param stream output stream
 */
void WGWalkPath(WGWalkOptions *options, const char *path, int depth, FILE *stream) {
  DIR *dir;
  struct dirent *dp;
  char *npath = NULL;
  
  if(!path || strlen(path) < 1)
    return;
  if(options->maxDepth >= 0 && depth > options->maxDepth)
    return;
  
  npath = (char *)malloc(strlen(path) + 1);
  if(path[strlen(path) - 1] == kPathSeparator){
    strncpy(npath, path, strlen(path) - 1);
    npath[strlen(path) - 1] = 0;
  }else{
    strcpy(npath, path);
  }
  
  if(!(dir = opendir(npath))){
    fprintf(stderr, "%s: Unable to open: %s (%s)\n", options->command, npath, strerror(errno));
    return;
  }
  
  while((dp = readdir(dir))){
    char *dpath = NULL;
    char *ppath = NULL;
    
    if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
      continue;
    if((options->flags & kOptionIgnoreHidden) == kOptionIgnoreHidden && strlen(dp->d_name) > 0 && dp->d_name[0] == '.')
      continue;
    
    dpath = (char *)malloc(strlen(npath) + strlen(dp->d_name) + 2);
    
    strcpy(dpath, npath);
    strcat(dpath, kPathSeparatorString);
    strcat(dpath, dp->d_name);
    
    if(dp->d_type & DT_DIR)
      WGWalkPath(options, dpath, depth + 1, stream);
    
    if((options->includeTypes & dp->d_type) == dp->d_type){
      
      if((options->flags & kOptionEscapeStrings) == kOptionEscapeStrings){
        size_t maxlen = strlen(dpath) * 2 + 1;
        ppath = (char *)malloc(maxlen);
        WGEscapeString(ppath, dpath, maxlen);
      }else{
        ppath = dpath;
      }
      
      // display the quote if needed
      if((options->flags & kOptionQuoteStrings) == kOptionQuoteStrings){
        fputc('"', stream);
      }
      
      // stat the file if we have to
      if((options->flags & kOptionRequiresStat) != 0){
        struct stat sb;
        
        if(stat(dpath, &sb) == -1){
          fprintf(stderr, "%s: Unable to stat file: %s\n", options->command, dpath);
          goto finish;
        }
        
        // display the file size, if necessicary
        if((options->flags & kOptionPrefixSize) == kOptionPrefixSize){
          fprintf(stream, "%d ", (unsigned int)sb.st_size);
        }
        
      }
      
      // display the path
      fputs(ppath, stream);
      
      // display the quote if needed
      if((options->flags & kOptionQuoteStrings) == kOptionQuoteStrings){
        fputc('"', stream);
      }
      
      // display the separator
      fputs(options->separator, stream);
      
    }
    
finish:
    if(dpath) free(dpath);
    
    if((options->flags & kOptionEscapeStrings) == kOptionEscapeStrings)
      if(ppath) free(ppath);
    
  }
  
  closedir(dir);
  if(npath) free(npath);
  
}


